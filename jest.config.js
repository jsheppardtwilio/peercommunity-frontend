module.exports = {
    "roots": [
      "<rootDir>/src"
    ],
    "transform": {
      "^.+\\.tsx?$": "ts-jest"
    },
    "moduleNameMapper": {
      "\\.(css|scss)$": "<rootDir>/tests/styleMock.js"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
    "globals": {
      "window": {},
      "document": {}
    },
    "testEnvironment": "jsdom",
    // Setup Enzyme
    "snapshotSerializers": ["enzyme-to-json/serializer"],
  }