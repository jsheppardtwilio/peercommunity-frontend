const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const path = require('path');

module.exports = merge(baseConfig, {
  mode: "development",
  devServer: {
    contentBase: path.join(__dirname, '/dist'),
    compress: true,
    host: '0.0.0.0',
    https: false,
    port: 3001,
    watchOptions: { aggregateTimeout: 300, poll: 1000 },
    historyApiFallback: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html'
    })
  ],
});
