
# setup
1. `yarn install`
2. `yarn dev`

# test
`yarn test --updateSnapshot`

# build
`yarn build`

# App Structure

`/components` folder contains the pages and components that make up the structure of the application

`/interfaces` folder contains some of the different TypeScript interfaces that we used when creating the MVP. These help us to strongly type parameters and variables that are passed around the application

`/styles` - self explanatory 😃

`/fixtures` folder is where our fake data lives. This would be coming from the API in the real world.

`/redux` folder is where all of the App state lives. At the moment it fetches the data from the `/fixtures` folder, but this will be from your API integrate the API endpoints here
