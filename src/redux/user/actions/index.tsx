import { ActionInterface } from '../../../interfaces';
import { UserActions, UserInterface } from '../../../interfaces/user';

export const loadUsers = (users: UserInterface[]): ActionInterface => {
  return {
    type: UserActions.USERS_LOADED,
    payload: { users }
  };
};
