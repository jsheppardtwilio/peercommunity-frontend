import { ActionInterface } from '../../../interfaces';
import { UserActions, UserStateInterface } from '../../../interfaces/user';

const initialState: UserStateInterface = {
  users: []
};

const UserReducer = (state = initialState, action: ActionInterface): UserStateInterface => {
  switch (action.type) {
    case UserActions.USERS_LOADED:
      return {
        ...state,
        users: action.payload.users
      };
    default:
      return state;
  }
};

export default UserReducer;
