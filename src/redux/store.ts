import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import ForumReducer from './forum/reducers';
import UserReducer from './user/reducers';
import { ForumStateInterface } from '../interfaces/forum';
import { UserStateInterface } from '../interfaces/user';

export interface AppStoreState {
  forum: ForumStateInterface;
  user: UserStateInterface;
}

// This is where the key of the reducers is set
const appReducer = combineReducers({
  forum: ForumReducer,
  user: UserReducer
});

const composeEnhancers =
  typeof window === 'object' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk),
);

const store = createStore(appReducer, enhancer);

export default store;
