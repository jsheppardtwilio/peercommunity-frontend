import { ActionInterface } from '../../../interfaces';
import { ChatroomInterface, ForumActions, ForumStateInterface } from '../../../interfaces/forum';
import moment from 'moment';

// This is the initial state value before any of the actions have been dispatched

const initialState: ForumStateInterface = {
  topics: [],
  chatrooms: [],
  selectedTopic: null,
  activeChat: null
};

// This reducer file houses all of the redux state
// The "cases" below are the different types that are
// dispatched by the actions in the ../actions files

const ForumReducer = (state = initialState, action: ActionInterface): ForumStateInterface => {
  switch (action.type) {
    case ForumActions.TOPICS_LOADED:
      return {
        ...state,
        topics: action.payload.topics
      };
    case ForumActions.CHATROOMS_LOADED:
      return {
        ...state,
        chatrooms: action.payload.chatrooms
      };
    case ForumActions.CHAT_LOADED:
      return {
        ...state,
        activeChat: action.payload.activeChat
      };
    case ForumActions.REFRESH_STORE:
      return {
        ...initialState
      };
    case ForumActions.SEND_MESSAGE:
      const { message, chatroomId } = action.payload;
      const { chatrooms } = state;
      const updatedChatrooms = chatrooms.map((chatroom: ChatroomInterface) => {
        if (chatroom.id === chatroomId) {
          return {
            ...chatroom,
            messages: [
              ...chatroom.messages,
              message
            ]
          };
        }

        return chatroom;
      });

      return {
        ...state,
        chatrooms: updatedChatrooms
      };
    case ForumActions.SEND_MESSAGE_IN_ACTIVE_CHAT:
      const { message: reply } = action.payload;
      const { activeChat } = state;
      if (activeChat) {
        const formattedDate = moment().format('YYYY-MM-DD\THH:mm:ss');
        activeChat.messages.push({
          date: formattedDate,
          id: formattedDate,
          userId: 'mockUserId',
          text: reply,
          userName: 'Yourself'
        });
        return {
          ...state,
          activeChat: {
            ...activeChat
          }
        };
      }
    default:
      return state;
  }
};

export default ForumReducer;
