import { ActionInterface } from '../../../interfaces';
import { ChatroomInterface, ForumActions, TopicInterface } from '../../../interfaces/forum';

// This is our fake data file. This wouldn't exist once an API had been integrated
import chatRooms from '../../../fixtures/forum/mock-chatrooms.json';

const MOCK_HTTP_TIME_LAPSE = 500;

// These actions are where you would call an API, and await the response.
// You would also perform your business logic here, such as filtering etc
// before dispatching the data to the reducer.

export const fetchChatroomsForTopic = (topicId: number) => (dispatch) => {
  setTimeout(() => {
    const chatrooms = chatRooms.filter((chatroom) => Number(chatroom.topicId) === topicId);

    // Here is where you would call and API to fetch the specific topics chat rooms
    // filtered based on the topicId param passed to the method.
    // By setting the payload as the response below, and dispatching the ForumActions.CHATROOMS_LOADED action type
    // it will be picked up by the reducer in /redux/reducers/index.tsx on line 19

    dispatch({
      type: ForumActions.CHATROOMS_LOADED,
      payload: { chatrooms }
    });
  }, MOCK_HTTP_TIME_LAPSE);
};

export const fetchChat = (chatId: number) => (dispatch) => {
  setTimeout(() => {
    const activeChat = chatRooms.find((chatroom) => Number(chatroom.id) === chatId);

    // Here is where you would call and API to fetch the active chat room's messages
    // filtered based on the chatId param passed to the method.
    // By setting the payload as the response below, and dispatching the ForumActions.CHAT_LOADED action type
    // it will be picked up by the reducer in /redux/reducers/index.tsx on line 24

    dispatch({
      type: ForumActions.CHAT_LOADED,
      payload: { activeChat }
    });
  }, MOCK_HTTP_TIME_LAPSE);
};
