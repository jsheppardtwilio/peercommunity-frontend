import { ServiceError } from './interface';

export interface ResponseError {
  status: number;
  data: {
    message: ServiceError
  };
}

const interServerError: ResponseError = {
  status: 500,
  data: {
    message: {
      code: 500,
      message: 'Connection corrupted'
    }
  }
};

export const hasData = (response: ResponseError | undefined) => {
  return response && response.data && response.data.message;
};

export function errorResponseHandler<ResultType>(response: ResponseError | undefined,
                                                 responseMapper: (errorMessage: string) => ResultType): ResultType {
  if (response === undefined) {
    response = interServerError;
  }
  const errorMessage =
    (hasData(response)) && response.data.message.message ?
      JSON.stringify(response.data.message.message) :
      hasData(response) ? JSON.stringify(response.data.message) : JSON.stringify(response);

  switch (response.status) {
    case 400:
      return responseMapper(errorMessage);
    case 404:
      return responseMapper(
        `Invalid endpoint configuration: 404 - ${ errorMessage }`);
    default:
      return responseMapper(
        `Internal server error: 500 - ${ errorMessage }`);
  }
}

