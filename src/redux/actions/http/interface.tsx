export interface ServiceError {
  code: number;
  message: string;
}
