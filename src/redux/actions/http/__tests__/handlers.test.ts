import * as Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { ResponseError, errorResponseHandler, hasData } from '../handlers';
import { ServiceError } from '../interface';

Enzyme.configure({ adapter: new Adapter() });

const mockResponseMapper = (message: string) => {
  return message;
};

describe('Http handlers', () => {

  it('hasData return message', () => {
    const message: ServiceError = { code: 500, message: 'test' };
    const response: ResponseError = { status: 500, data: { message } };
    expect(hasData(response)).toBe(message);
  });

  it('hasData return undefined', () => {
    expect(hasData(undefined)).toBe(undefined);
  });

  it('errorResponseHandler: response 400', () => {
    const message: ServiceError = { code: 400, message: 'testing 400' };
    const response: ResponseError = { status: 400, data: { message } };
    expect(errorResponseHandler(response, mockResponseMapper)).toBe(JSON.stringify(message.message));
  });

  it('errorResponseHandler: response 404', () => {
    const message: ServiceError = { code: 404, message: 'testing 401' };
    const response: ResponseError = { status: 404, data: { message } };
    expect(errorResponseHandler(response, mockResponseMapper))
      .toBe(`Invalid endpoint configuration: 404 - ${JSON.stringify(message.message)}`);
  });

  it('errorResponseHandler: response 500', () => {
    const message: ServiceError = { code: 502, message: 'testing 502' };
    const response: ResponseError = { status: 502, data: { message } };
    expect(errorResponseHandler(response, mockResponseMapper))
      .toBe(`Internal server error: 500 - ${JSON.stringify(message.message)}`);
  });

  it('errorResponseHandler: response 500', () => {
    const message: ServiceError = { code: 500, message: 'testing 500' };
    const response: ResponseError = { status: 500, data: { message } };
    expect(errorResponseHandler(response, mockResponseMapper))
      .toBe(`Internal server error: 500 - ${JSON.stringify(message.message)}`);
  });

  it('errorResponseHandler: response 500 - no response', () => {
    expect(errorResponseHandler(undefined, mockResponseMapper))
      .toBe(`Internal server error: 500 - ${JSON.stringify('Connection corrupted')}`);
  });

});
