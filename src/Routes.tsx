import React from 'react';
import { ConnectedComponent } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from './components/HomePage';
import ChatRoom from './components/ChatRoom';
import Sidebar from './components/Navigator';
import { CssBaseline, Hidden } from '@material-ui/core';
import Header from './components/Header';
import theme from './Theme';

import {
    ThemeProvider,
    WithStyles,
    createMuiTheme,
    createStyles,
    withStyles,
} from '@material-ui/core/styles';

import './App.scss';

interface RoutesInterface {
    path: string;
    component?: ConnectedComponent<any, any>;
}
// on use of 'any'^: 'connect()' returns a connected component as a result
// each connected component has unique types and props

const drawerWidth = 256;

const styles = createStyles({
    root: {
        display: 'flex',
        minHeight: '100vh',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    app: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    main: {
        flex: 1,
        padding: theme.spacing(2, 1),
        background: '#009be5',
        paddingBottom: '50px'
    },
    footer: {
        padding: theme.spacing(2),
        background: '#eaeff1',
    },
});

const routes: RoutesInterface[] = [
    {
        path: '/forum/:topicId',
        component: HomePage
    },
    {
        path: '/chat/:chatId',
        component: ChatRoom
    },
    {
        path: '/',
        component: HomePage
    }
];

export interface PaperbaseProps extends WithStyles<typeof styles> { }

const Routes = (props: any) => {

    const { classes } = props;

    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter basename={'/'}>
                <div className={classes.root}>
                    <CssBaseline />
                    <nav className={classes.drawer}>
                        <Hidden smUp implementation='js'>
                            <Sidebar
                                PaperProps={{ style: { width: drawerWidth } }}
                                variant='temporary'
                                open={mobileOpen}
                                onClose={handleDrawerToggle}
                                onOpen={handleDrawerToggle}
                            />
                        </Hidden>
                        <Hidden xsDown implementation='css'>
                            <Sidebar PaperProps={{ style: { width: drawerWidth } }}
                                open={mobileOpen}
                                onClose={handleDrawerToggle}
                                onOpen={handleDrawerToggle} />
                        </Hidden>
                    </nav>
                    <div className={classes.app}>
                        <Header activeTopic={'123'} onDrawerToggle={handleDrawerToggle} />
                        <main className={classes.main}>
                            <Switch>
                                {routes.map((route: any, index: number) => {
                                    return (<Route {...route} key={index} />);
                                })}
                            </Switch>
                        </main>
                    </div>
                </div>

            </BrowserRouter>
        </ThemeProvider>
    );

};

export default withStyles(styles)(Routes);
