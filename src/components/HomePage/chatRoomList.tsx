import React from 'react';
import PropTypes from 'prop-types';
import { Button, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import ChatBubbles from '../ChatRoom/chatBubbles/index';
import LaunchIcon from '@material-ui/icons/Launch';

const LATEST_MESSAGE_AMOUNT = 3;

class ChatRoomListComponent extends React.Component<any> {
  public static propTypes = {
    chatrooms: PropTypes.array.isRequired,
    dispatch: PropTypes.object.isRequired
  };
  public render() {
    const { openChat } = this.props.dispatch;
    return (
      <div>
        <List component={'nav'}>
          {this.props.chatrooms.map((chatroom) => (
            <div key={chatroom.id} style={{ backgroundColor: 'rgb(70, 179, 230)', borderRadius: '5px' }}>
              <ListItem button key={chatroom.id} onClick={() => openChat(chatroom.id)}>
                <ListItemText primary={chatroom.name} style={{ color: '#ffffff', fontSize: '36px', fontStyle: 'bold' }} />
                <LaunchIcon style={{ color: '#ffffff' }} />
              </ListItem>
              <div style={{
                borderRadius: '3px',
                padding: '10px',
                marginBottom: '10px'
              }}>
                <ChatBubbles messages={chatroom.messages.slice(0, LATEST_MESSAGE_AMOUNT)} />
                {this.renderMoreChat(chatroom)}
              </div>

            </div>
          ))}
        </List>
      </div>
    );
  }

  private renderMoreChat(chat) {
    const { openChat } = this.props.dispatch;
    if (chat.messages && chat.messages.length <= LATEST_MESSAGE_AMOUNT) { return null; }
    return (
      <Button variant={'contained'} color={'primary'} fullWidth style={{ marginTop: '20px' }} onClick={() => openChat(chat.id)}>
        More on this chat
      </Button>
    );
  }
}

export default ChatRoomListComponent;
