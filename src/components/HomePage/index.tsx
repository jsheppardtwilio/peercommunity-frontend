import React, { Component } from 'react';
import { connect, shallowEqual } from 'react-redux';
import { fetchChatroomsForTopic } from '../../redux/forum/actions';
import ChatRoomListComponent from './chatRoomList';
import { ForumActions } from '../../interfaces/forum';
import { useHistory } from 'react-router';


class HomePageComponent extends Component<any> {

  public render() {
    const handleOpenChat = (chatId: string) => {
      if (chatId) {
        this.props.history.push(`/chat/${chatId}`);
      }
    };

    return (
      <div>
        {this.props.match.params.topicId ? null : <h1>Welcome to turn2me. Please select a topic first.</h1>}
        <ChatRoomListComponent chatrooms={this.props.chatrooms} dispatch={{ openChat: handleOpenChat }} />
      </div>
    );
  }

  public componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<{}>, snapshot?: any): void {
    if (this.props.match.params.topicId !== prevProps.match.params.topicId) { this.fetchChatRooms(); }
  }

  public componentDidMount(): void {
    this.fetchChatRooms();
  }

  private fetchChatRooms(): void {
    const topicId = this.props.match.params.topicId;
    this.props.refreshState();
    this.props.fetchChatroomsForTopic(Number(topicId));
  }
}

const HomePage = connect(
  (state: any) => {
    return {
      chatrooms: state.forum.chatrooms
    };
  },
  (dispatch: any) => {
    return {
      fetchChatroomsForTopic: (topicId) => dispatch(fetchChatroomsForTopic(topicId)),
      refreshState: () => dispatch({ type: ForumActions.REFRESH_STORE })
    };
  }
)(HomePageComponent);

export default HomePage;
