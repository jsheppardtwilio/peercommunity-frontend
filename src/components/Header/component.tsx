import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { Theme, WithStyles, createStyles, withStyles } from '@material-ui/core/styles';
const profilePic = require('./profile.png');

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = (theme: Theme) =>
  createStyles({
    secondaryBar: {
      zIndex: 0,
    },
    menuButton: {
      marginLeft: -theme.spacing(1),
    },
    iconButtonAvatar: {
      padding: 4,
    },
    link: {
      'textDecoration': 'none',
      'color': lightColor,
      '&:hover': {
        color: theme.palette.common.white,
      },
    },
    button: {
      borderColor: lightColor,
    },
    semiOpaque: {
      opacity: 0.5,
      fontSize: '14px'
    }
  });

interface HeaderProps extends WithStyles<typeof styles> {
  onDrawerToggle: () => void;
  activeTopic: string;
}

function Header(props: HeaderProps) {
  const { classes, onDrawerToggle } = props;

  const children = [
    { id: 1, name: 'Say Hi!', active: true },
    { id: 2, name: 'Relationships' },
    { id: 3, name: 'Family' },
    { id: 4, name: 'School' },
    { id: 5, name: 'Feeling Stressed' },
    { id: 6, name: 'Feeling Good' },
    { id: 7, name: 'Feeling Down' },
    { id: 8, name: 'Grief and Loss' },
    { id: 9, name: 'Problem Solving' }
  ];

  const uriData = window.location.pathname.split('/');

  return (
    <React.Fragment>
      <AppBar color='primary' position='sticky' elevation={2} style={{
        height: '88px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
      }}>
        <Toolbar style={{ width: '100%' }}>
          <Grid container spacing={1} alignItems='center'>
            <Hidden smUp>
              <Grid item>
                <IconButton
                  color='inherit'
                  aria-label='open drawer'
                  onClick={onDrawerToggle}
                  className={classes.menuButton}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Hidden>
            <Grid item sm={10} xs={8}>
              <Typography color='inherit' variant='h5' component='h1'>
                {uriData[1] === 'forum' ? children[Number(uriData[2]) - 1].name : 'Thread'} <div className={classes.semiOpaque}>{uriData[1] !== 'forum' ? '' : 'Channel'}</div>
              </Typography>
            </Grid>
            <Grid item sm={1} xs={1} alignItems={'flex-end'}>
              <Tooltip title='Alerts • No alters'>
                <IconButton color='inherit'>
                  <NotificationsIcon />
                </IconButton>
              </Tooltip>
            </Grid>
            <Grid item sm={1} xs={1} alignItems={'flex-end'}>
              <IconButton color='inherit' className={classes.iconButtonAvatar}>
                <Avatar src={profilePic} alt='My Avatar' />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </React.Fragment >
  );
}

export default withStyles(styles)(Header);
