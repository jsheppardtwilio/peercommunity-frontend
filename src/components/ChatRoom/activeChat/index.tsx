import React from 'react';
import PropTypes from 'prop-types';
import ChatBubblesComponent from '../chatBubbles';
import { AppBar, Button, Grid, Hidden, TextField, Toolbar } from '@material-ui/core';
import './style.scss';

class ActiveChatComponent extends React.Component<any, any> {
  public static propTypes = {
    chat: PropTypes.object.isRequired,
    dispatch: PropTypes.object.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { replyMessage: '' };
  }
  public render() {
    return (
      <div>
        <ChatBubblesComponent messages={this.props.chat.messages} />
        {this.renderSendMessage()}
      </div>
    );
  }

  private renderSendMessage() {
    const { sendMessage } = this.props.dispatch;
    if (!this.props.chat) { return null; }
    const handleChange = (name) => (event) => {
      this.setState({
        [name]: event.target.value
      });
    };
    return (
      <React.Fragment>
        <Hidden smUp>
          <AppBar style={{ backgroundColor: '#ffffff', top: 'unset', bottom: 0 }}>
            <Toolbar>
              <Grid container spacing={2}>
                <Grid item xs={9}>
                  <TextField
                    multiline
                    fullWidth
                    placeholder={'Reply here...'}
                    onChange={handleChange('replyMessage')}
                    value={this.state.replyMessage}
                  />
                </Grid>
                <Grid item xs={3}>
                  <Button fullWidth variant={'contained'} color={'primary'} onClick={
                    () => {
                      sendMessage(this.state.replyMessage);
                      this.setState({
                        replyMessage: ''
                      });
                    }}>
                    Send
              </Button>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
        </Hidden>
        <Hidden xsDown>
          <AppBar position='fixed' style={{ backgroundColor: '#ffffff', top: 'unset', bottom: 0, position: 'absolute', zIndex: 99999 }} >
            <Toolbar>
              <Grid container spacing={2}>
                <Grid item xs={9}>
                  <TextField
                    multiline
                    fullWidth
                    placeholder={'Reply here...'}
                    onChange={handleChange('replyMessage')}
                    value={this.state.replyMessage}
                  />
                </Grid>
                <Grid item xs={3}>
                  <Button fullWidth variant={'contained'} color={'primary'} onClick={() => {
                    sendMessage(this.state.replyMessage);
                    this.setState({
                      replyMessage: ''
                    });
                  }}>
                    Send
              </Button>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
        </Hidden>
      </ React.Fragment>
    );
  }
}

export default ActiveChatComponent;
