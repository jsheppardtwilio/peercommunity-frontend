import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import moment from 'moment';
import { Button } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';

const TEMP_SENDER_ID = 'mockUserId';

class ChatBubblesComponent extends React.Component<any> {
  public static propTypes = {
    messages: PropTypes.array.isRequired
  };
  public render() {
    return (
      <div className={'chat-bubble-component'}>
        {this.props.messages && this.props.messages.map((message) => {
          return (
            <div className={message.userId === TEMP_SENDER_ID ? 'bubble right animated fadeIn' : 'bubble left animated fadeIn'} key={message.id}>
              <div className={'bubble-content'} style={{ position: 'relative' }}>
                <span className={'username'}>{message.userName}</span>
                <span className={'message'}>{message.text}</span>
                <span className={'time'}>{moment(message.date).format('HH:mm')}</span>
                <Button color='primary' aria-label='add' style={{ width: '20%', backgroundColor: '#ff000050', color: '#ffffff' }}>
                  <FavoriteIcon /> {' ' + Math.floor(Math.random() * 100)}
                </Button>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default ChatBubblesComponent;
