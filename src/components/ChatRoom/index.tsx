import React from 'react';
import { connect } from 'react-redux';
import { fetchChat } from '../../redux/forum/actions';
import ActiveChatComponent from './activeChat/index';
import { ForumActions } from '../../interfaces/forum';
import CircularProgress from '@material-ui/core/CircularProgress';
import Toolbar from '@material-ui/core/Toolbar';

class ChatRoomComponent extends React.Component<any> {

  public render() {
    return (
      <div>
        <h1 style={{ margin: 0, padding: '0px 20px 20px 25px', color: '#ffffff' }}>{this.props.forumState.activeChat ? this.props.forumState.activeChat.name : <CircularProgress style={{ color: '#ffffff', left: '50%', position: 'relative', marginLeft: '-20px' }} />}</h1>
        {
          this.props.forumState.activeChat &&
          <div className={'chat-container'}>
            <ActiveChatComponent
              chat={this.props.forumState.activeChat}
              dispatch={{ sendMessage: this.props.sendMessage }}
            />
          </div>
        }
      </div>
    );
  }

  public componentDidMount(): void {
    this.fetchChat();
  }

  private fetchChat(): void {
    const chatId = this.props.match.params.chatId;
    this.props.refreshState();
    this.props.fetchChat(Number(chatId));
  }
}

const ChatRoom = connect(
  (state: any) => {
    return {
      forumState: state.forum
    };
  },
  (dispatch: any) => {
    return {
      fetchChat: (topicId) => dispatch(fetchChat(topicId)),
      refreshState: () => dispatch({ type: ForumActions.REFRESH_STORE }),
      sendMessage: (message) => dispatch({
        type: ForumActions.SEND_MESSAGE_IN_ACTIVE_CHAT,
        payload: { message }
      }),
    };
  }
)(ChatRoomComponent);

export default ChatRoom;
