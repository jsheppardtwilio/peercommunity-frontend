import * as Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as EnzymeToJson from 'enzyme-to-json';
import * as React from 'react';
import { Alert } from '../../Alerts';

Enzyme.configure({ adapter: new Adapter() });

const defaultParams = { text: [], type: 'info', close: false };

describe('Alert Helper', () => {

  it('Render Alert', () => {
    const output = Enzyme.shallow(<Alert {...defaultParams} />);
    expect(EnzymeToJson.shallowToJson(output)).toMatchSnapshot();
  });

  it('Alert With messages', () => {
    const params = { ...defaultParams, ...{ text: ['testing', 'testing 2'] } };
    const output = Enzyme.shallow(<Alert {...params} />);
    expect(EnzymeToJson.shallowToJson(output)).toMatchSnapshot();
  });

  it('Alert With error type', () => {
    const params = { ...defaultParams, ...{ type: 'error' } };
    const output = Enzyme.shallow(<Alert {...params} />);
    expect(EnzymeToJson.shallowToJson(output)).toMatchSnapshot();
  });

});
