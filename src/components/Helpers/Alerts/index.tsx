import React from 'react';

const checkType = (type: string) => {
  if (type === 'error') { type = 'danger'; }
  return type;
};

const renderAlert = (text: any[], type: string, close: any) => {
  if (text === null || text.length === 0) {
    return null;
  }

  return (
    <div className={'alert alert-' + checkType(type) + (close ? ' alert-dismissible' : '')} role='alert'>
      {close ?
        <button type='button' className='close' onClick={close}
          data-dismiss='alert' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
        : ''
      }
      {text.map((t, index) => {
        return <span key={index}>{t} <br /></span>;
      })}
    </div>);
};

export const Alert = ({
  text = [],
  type = 'info',
  close = false
}: any) => (
    renderAlert(text, type, close)
  );
