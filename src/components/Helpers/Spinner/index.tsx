import React from 'react';

import './styles.scss';

interface SpinnerProps {
    size?: 'small' | 'large';
}

const Spinner = ({ size }: SpinnerProps) => {
    const classes = ['helper_spinner'];
    if (size) {
        classes.push(`helper_spinner__${size}`);
    }

    return <div className={classes.join(' ')} />;
};

export default Spinner;
