import React from 'react';
import { Theme, WithStyles, createStyles, withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { DrawerProps } from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import clsx from 'clsx';
import DnsRoundedIcon from '@material-ui/icons/DnsRounded';
import PermMediaOutlinedIcon from '@material-ui/icons/PhotoSizeSelectActual';

import SayHiIcon from '@material-ui/icons/EmojiPeople';
import RelationshipsIcon from '@material-ui/icons/Favorite';
import PeopleIcon from '@material-ui/icons/People';
import SchoolIcon from '@material-ui/icons/School';
import HealthIcon from '@material-ui/icons/Healing';

import StressedIcon from '@material-ui/icons/Waves';
import HappyIcon from '@material-ui/icons/SentimentVerySatisfied';
import SadIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import ProblemSolvingIcon from '@material-ui/icons/Extension';
import GriefIcon from '@material-ui/icons/NaturePeople';

import TimerIcon from '@material-ui/icons/Favorite';
import SettingsIcon from '@material-ui/icons/Settings';
import PhonelinkSetupIcon from '@material-ui/icons/PhonelinkSetup';
import { Omit } from '@material-ui/types';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

const logo = require('./t2m.png'); // Tell Webpack this JS file uses this image

const categories = [
    {
        id: 'Active Channels',
        children: [
            { id: 1, name: 'Say Hi!', icon: <SayHiIcon /> },
            { id: 2, name: 'Relationships', icon: <RelationshipsIcon /> },
            { id: 3, name: 'Family', icon: <PeopleIcon /> },
            { id: 4, name: 'School', icon: <SchoolIcon /> },
            { id: 5, name: 'Feeling Stressed', icon: <StressedIcon /> },
            { id: 6, name: 'Feeling Good', icon: <HappyIcon /> },
            { id: 7, name: 'Feeling Down', icon: <SadIcon /> },
            { id: 8, name: 'Grief and Loss', icon: <GriefIcon /> },
            { id: 9, name: 'Problem Solving', icon: <ProblemSolvingIcon /> }
        ],
    },
    {
        id: 'Helpful Links',
        children: [
            { id: 0, name: 'Talk To Us', icon: <SettingsIcon /> },
            { id: 0, name: 'Other Services', icon: <TimerIcon /> },
            { id: 0, name: 'Logout', icon: <PhonelinkSetupIcon /> },
        ],
    },
] as any;

const styles = (theme: Theme) =>
    createStyles({
        categoryHeader: {
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
        },
        categoryHeaderPrimary: {
            color: theme.palette.common.white,
        },
        item: {
            'paddingTop': 1,
            'paddingBottom': 1,
            'color': 'rgba(255, 255, 255, 0.7)',
            '&:hover,&:focus': {
                backgroundColor: 'rgba(255, 255, 255, 0.08)',
            },
        },
        itemCategory: {
            backgroundColor: '#232f3e',
            boxShadow: '0 -1px 0 #404854 inset',
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
        },
        firebase: {
            fontSize: 24,
            color: theme.palette.common.white,
        },
        itemActiveItem: {
            color: '#4fc3f7',
        },
        itemPrimary: {
            fontSize: '20px',
        },
        itemIcon: {
            minWidth: 'auto',
            marginRight: theme.spacing(2),
        },
        divider: {
            marginTop: theme.spacing(2),
        },
    });


function Navigator(props: any) {

    const redirectToTopic = (topicId: number) => {
        if (topicId > 0) {
            props.onClose();
            props.history.push(`/forum/${topicId}`);
        }
    };

    const { classes, staticContext, ...other } = props;

    return (
        <SwipeableDrawer variant='permanent'
            {...other}>
            <List disablePadding>
                <ListItem
                    onClick={(e) => {
                        props.history.push(`/forum/1`);
                    }}
                    className={clsx(classes.firebase, classes.item, classes.itemCategory)}
                >
                    <img src={logo} style={{ width: '100%' }} />
                </ListItem>
                {categories.map(({ id, children }) => (
                    <React.Fragment key={id}>
                        <ListItem className={classes.categoryHeader}>
                            <ListItemText
                                classes={{
                                    primary: classes.categoryHeaderPrimary,
                                }}
                            >
                                {id}
                            </ListItemText>
                        </ListItem>
                        {children.map(({ id: topicId, name: childId, icon, active }) => (
                            <ListItem
                                key={childId}
                                button
                                className={clsx(classes.item, active && classes.itemActiveItem)}
                                onClick={(e) => { redirectToTopic(topicId); }}
                            >
                                <ListItemIcon className={classes.itemIcon}>{icon}</ListItemIcon>
                                <ListItemText
                                    classes={{
                                        primary: classes.itemPrimary,
                                    }}
                                >
                                    {childId}
                                </ListItemText>
                            </ListItem>
                        ))}
                        <Divider className={classes.divider} />
                    </React.Fragment>
                ))}
            </List>
        </SwipeableDrawer>
    );
}

export default withStyles(styles)(withRouter(Navigator));
