import { useEffect, useState } from 'react';

export const useScroll = () => {
    const [scrollId, setScrollId] = useState(0);
    const [scrollType, setScrollType] = useState<'auto' | 'smooth'>('auto');

    useEffect(() => {
        switch (scrollId) {
            case 1:
                try {
                    window.scroll({
                        top: 0,
                        left: 0,
                        behavior: scrollType,
                    });
                } catch (error) {
                    window.scrollTo(0, 0);
                }

                setScrollId(0);
        }
    }, [scrollId, scrollType]);

    const scrollToTop = (type: 'smooth' | 'auto' = 'auto') => {
        setScrollType(type);
        setScrollId(1);
    };

    return [scrollToTop];
};
