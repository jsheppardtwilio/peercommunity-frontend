export enum UserActions {
    USERS_LOADED = 'USERS_LOADED'
}

export interface UserInterface {
    id: string;
    username: string;
    email: string;
    phoneNumber: string;
}

export interface UserStateInterface {
    users: UserInterface[];
}
