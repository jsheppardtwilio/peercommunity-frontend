export enum ForumActions {
    TOPICS_LOADED = 'TOPICS_LOADED',
    CHATROOMS_LOADED = 'CHATROOMS_LOADED',
    SEND_MESSAGE = 'SEND_MESSAGE',
    SEND_MESSAGE_IN_ACTIVE_CHAT = 'SEND_MESSAGE_IN_ACTIVE_CHAT',
    CHAT_LOADED = 'CHAT_LOADED',
    REFRESH_STORE = 'REFRESH_STORE'
}

export interface MessageInterface {
    id: string;
    userId: string;
    userName: string;
    text: string;
    date: string;
}

export interface TopicInterface {
    id: string;
    name: string;
}

export interface ChatroomInterface {
    id: string;
    name: string;
    topicId: string;
    messages: MessageInterface[];
}

export interface ForumStateInterface {
    topics: TopicInterface[];
    chatrooms: ChatroomInterface[];
    selectedTopic: TopicInterface | null;
    activeChat: ChatroomInterface | null;
}
