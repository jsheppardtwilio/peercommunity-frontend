import React from 'react';
import { Provider } from 'react-redux';
import Routes from './Routes';
import store from './redux/store';

class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <Routes forum={store.getState().forum} />
      </Provider>
    );
  }
}

export default App;
